#### Contents
- [Messaging Queues](#-messaging-queues)
  - [What are Messaging Queues?](#what-are-messaging-queues)
  - [Why they are used?](#why-they-are-used)
  - [What are popular tools?](#what-are-popular-tools)
  - [What is Enterprise Message Bus?](#what-is-enterprise-message-bus)
- [Reference](#reference)



# **Messaging Queues**
-------------------------------------
### **What are Messaging Queues?**
  - A message queue is a form of asynchronous service-to-service communication used in serverless and microservices architectures.
  - Messages are stored on the queue until they are processed and deleted.
  - Each message is processed only once, by a single consumer.

![Image](image/image1.png)

### Why they are used?
  
  - **Simplified Decoupling**
      - Message Queues remove dependencies between components and significantly simplify the coding of decoupled applications.
      - Message Queues are an elegantly simple way to decoupled distributed systems, whether you're using monolithic applications, microservices, or serverless architectures.
  - **Better Performance**
      - Message queues enable asynchronous communication, which means that the endpoints that are producing and consuming messages interact with the queue, not each other.
      - Producers can add requests to the queue without waiting for them to be processed.
      - Consumers process messages only when they are available.
      - No component in the system is ever stalled waiting for another, optimizing data flow.
  - **Increased Reliability**
      - Queues make your data persistent and reduce the errors that happen when different parts of your system go offline.
      - By separating different components with message queues, you can create more fault tolerance.
      - If one part of the system is unreachable, the other can continue to interact with the queue.
      - The queue itself can also be mirrored for even more availability.
  - **Granular Scalability**
      - Message queues make it possible to scale precisely where you need to.
      - When workloads peak, multiple instances of your application can all add requests to the queue without risk of collision.
      - Producers, consumers, and the queue itself can all grow and shrink on demand.

### What are popular tools?
  - **Kafka** 
      - It is a distributed, partitioned, replicated commit log service.
      - It provides the functionality of a messaging system, but with a unique design.
  - **RabbitMQ**
       -  It gives your applications a common platform to send and receive messages, and your messages a safe place to live until received.
  - **Amazon SQS**
      - Transmit any volume of data, at any level of throughput, without losing messages or requiring other services to be always available.
      - With SQS, you can offload the administrative burden of operating and scaling a highly available messaging cluster.
  - **Celery**
      - It is an asynchronous task queue/job queue based on distributed message passing.
      - It is focused on real-time operation but supports scheduling as well.
  - **ActiveMQ**
      - Apache ActiveMQ is fast, supports many Cross Language Clients and Protocols, comes with easy to use Enterprise Integration Patterns
      - Many advanced features will fully support JMS 1.1 and J2EE 1.4.
      - Apache ActiveMQ is released under the Apache 2.0 License.

### What is Enterprise Message Bus?
  - An enterprise contains several existing systems that must be able to share data and operate in a unified manner in response to a set of common business requests.
  - A Enterprise Message Bus is a combination of a common data model, a common command set, and a messaging infrastructure to allow different systems to communicate through a shared set of interfaces.
  - This is analogous to a communications bus in a computer system, which serves as the focal point for communication between the CPU, main memory, and peripherals.



## **Reference**

  - [https://aws.amazon.com/message-queue/](https://aws.amazon.com/message-queue/)
  - [https://aws.amazon.com/message-queue/benefits/](https://aws.amazon.com/message-queue/benefits/)
  - [https://stackshare.io/message-queue](https://stackshare.io/message-queue)
  - [https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageBus.html](https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageBus.html)
